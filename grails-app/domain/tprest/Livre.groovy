package tprest

class Livre {

    String name
    Date releaseDate
    String isbn
    String author

    static beLongsTo = [bibliotheque:Bibliotheque]

    static constraints = {
        name blank:false
        releaseDate nullable:false
        isbn null:false
        author blank:false
    }

}
