package tprest

class Bibliotheque {
    String name
    String address
    Integer yearCreated

    static hasMany = [livres:Livre]

    static constraints = {
        name blank:false
        address blank:false
        yearCreated nullable: false
    }

    static mapping = {
        livres:"all-delete-orphan"
    }


}
