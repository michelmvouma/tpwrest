import tprest.Bibliotheque
import tprest.Livre

class BootStrap {

    def init = { servletContext ->
        Bibliotheque biblio1 = new Bibliotheque(name: "Bibliotheque centre", address: "06 400 Nice France", yearCreated:2005)
        Bibliotheque biblio2 = new Bibliotheque(name: "Centre commercial du bail", address: "06 400 Nice Cameroun", yearCreated:2005)
        biblio1.save(flush:true, failOnError:true)
        biblio2.save(flush:true, failOnError:true)

        String dateChaineLivre1 = '12-01-2014'

        Date dateLivre1 = Date.parse("dd-MM-yyyy" , dateChaineLivre1)
        String dateChaineLivre2 = '12-01-2016'
        Date dateLivre2 = Date.parse("dd-MM-yyyy" , dateChaineLivre2)

        Livre livre1 = new Livre(name: "Madame du champ", isbn: "1548ABMLD", author: "Pierre DuVal",
                releaseDate: dateLivre1)
        Livre livre2 = new Livre(name: "Conte et Légende d'afrique", isbn: "ABGF1254", author: "Michel Mvamvou",
                releaseDate: dateLivre2)
        Livre livre3 = new Livre(name: "Super roi", isbn: "ABlm1259", author: "Serge du Champ",
                releaseDate: dateLivre2)

        biblio1.addToLivres(livre1)
        biblio1.addToLivres( livre2)
        biblio2.addToLivres(livre3)

        livre1.save(flush:true, failOnError:true)
        livre2.save(flush:true, failOnError:true)
        livre3.save(flush:true, failOnError:true)
        biblio1.save(flush:true, failOnError:true)
        biblio2.save(flush:true, failOnError:true)
    }
    def destroy = {
    }
}
