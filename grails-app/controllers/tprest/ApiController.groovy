package tprest

import grails.converters.JSON
import groovy.json.JsonBuilder
import groovy.util.logging.Log


class ApiController {

    def String CONTENT_APPLICATION = "application/json"
    def String MIME_ACCEPT="text"
    def String SUCCES_REQUETTE= "200"
    def String RESSOURCE_CREE = "201"
    def String AUCUNE_RESSOURCE_TROUVEE ="404"
    def String METHODE_NON_ACCEPTEE = "405"
    def String RESSOURCE_INDISPONIBLE_AU_FORMAT_ACCEPT = "406"




    /**
     * Ajouter un livre
     * Retourne un objet json livre
     * @return
     */
    def livre()
    {
        Livre livre = Livre.get(params.id)
        //println("accept " + request.accept)
        switch (request.method)
        {
            case "GET":

                if(livre==null)
                {
                    response.status=404
                    render status: AUCUNE_RESSOURCE_TROUVEE
                }else{
                    boolean resultat = rechercheJsonEntete(request, CONTENT_APPLICATION)
                    //println("resultat " + resultat)
                    if(resultat)
                    {
                        response.status =200
                        response.setContentType(CONTENT_APPLICATION)
                        JsonBuilder builder = new JsonBuilder()
                        builder(
                                nom: livre.getName(),
                                isbn: livre.getIsbn(),
                                author: livre.getAuthor(),
                                id:livre.getId(),
                                releaseDate:livre.getReleaseDate()
                        )
                        render builder, status:SUCCES_REQUETTE

                    }else {
                        response.status=406
                        render status: RESSOURCE_INDISPONIBLE_AU_FORMAT_ACCEPT
                    }
                }
                break

            case "PUT":
                if(livre==null)
                {

                    response.status=404
                    render status: AUCUNE_RESSOURCE_TROUVEE
                }else{

                    try{

                        Date dateLivre = Date.parse("dd-MM-yyyy", params.releaseDate)
                        livre.setReleaseDate(dateLivre)
                        livre.setName(params.name)
                        livre.setIsbn(params.isbn)
                        livre.setAuthor(params.author)
                        if(!livre.save())
                        {
                            response.status=404
                            render status:  AUCUNE_RESSOURCE_TROUVEE
                        }else{
                            response.status=200
                            render status: SUCCES_REQUETTE, text:"Success"

                        }

                    }catch(Exception ex)
                    {
                        response.status=404
                        render status:  AUCUNE_RESSOURCE_TROUVEE
                    }
                }
                break

            case "DELETE":

                if(livre==null)
                {
                    response.status=404
                    render status: AUCUNE_RESSOURCE_TROUVEE, text:"L'entité à supprimer est introuvable"
                }else{

                    Bibliotheque b
                    Bibliotheque.findAll(livres : livre).each {
                        if( Bibliotheque.get(it.id).livres.contains(livre)){
                            Bibliotheque.get(it.id).removeFromLivres(livre)
                        }
                    }

                    livre.delete()
                    response.status=200
                    render status: SUCCES_REQUETTE, text:"OK"
                }
                break


            default:
                response.status=405
                render status: METHODE_NON_ACCEPTEE
        }
    }


    /**
     * obtenir la liste des livres
     * @return
     */
    def livres()
    {
        switch (request.method)
        {
            case "GET":
                boolean resultat = rechercheJsonEntete(request, CONTENT_APPLICATION)
                if(resultat)
                {
                    response.status =200
                    List listLivres = Livre.list()
                    def all = listLivres.collect {Livre livre ->
                        [nom: livre.getName(),
                         isbn: livre.getIsbn(),
                         author: livre.getAuthor(),
                         id:livre.getId(),
                         releaseDate:livre.getReleaseDate()]
                    }.sort() { it.name }

                    response.status=200
                    render all as JSON
                    //render arrayJson, status:SUCCES_REQUETTE
                }else{
                    response.status=406
                    render status: RESSOURCE_INDISPONIBLE_AU_FORMAT_ACCEPT
                }
                break

            case "POST":
                Date dateLivre2 = Date.parse("dd-MM-yyyy" , params.releaseDate)
                Livre l = new Livre(name : params.name, releaseDate: dateLivre2, isbn:  params.isbn, author:  params.author)
                Bibliotheque b = Bibliotheque.get(params.bId)
                b.addToLivres(l)

                if(!l.save(flush:true, failOnError:true))
                {
                    response.status=404
                    render status: AUCUNE_RESSOURCE_TROUVEE
                }else{
                    b.save(flush:true, failOnError:true)
                    response.status=201
                    render status: RESSOURCE_CREE, text:"Success"
                }
                break

            default:
                response.status=405
                render status: METHODE_NON_ACCEPTEE
        }
    }








    /**
     * Obtenir la liste des bibliothèques
     * @return
     */
    def bibliotheques()
    {
        log.println("########## "+ params + "#########")
        switch (request.method)
        {
            case "GET":
                boolean resultat = rechercheJsonEntete(request, CONTENT_APPLICATION)
                if(resultat)
                {
                    response.status =200
                    def JsonBuilder[] arrayJson = new JsonBuilder[Bibliotheque.list().size()]
                    List listBiblio = Bibliotheque.list()

                    def all = listBiblio.collect {Bibliotheque b ->
                        [nom: b.getName(),
                         id: b.getId(),
                         address: b.getAddress(),
                         yearCreated:b.getYearCreated(),
                         livres : b.livres   ]
                    }.sort() { it.name }

                    response.status=200
                    render all as JSON
                    //render arrayJson, status:SUCCES_REQUETTE
                }else{
                    response.status=406
                    render status: RESSOURCE_INDISPONIBLE_AU_FORMAT_ACCEPT
                }

                break

            case "POST":

                Bibliotheque b = new Bibliotheque(params)
                if(!b.save(flush:true, failOnError:true))
                {
                    response.status=404
                    render status: AUCUNE_RESSOURCE_TROUVEE
                }else{
                    response.status=201
                    render status: RESSOURCE_CREE, text:"Success"
                }
                break

            default:
                response.status=405
                render status: METHODE_NON_ACCEPTEE
        }
    }


    def bibliotheque()
    {
        Bibliotheque bibliotheque = Bibliotheque.get(params.id)
        //println("accept " + request.accept)
        switch (request.method)
        {
            case "GET":

                if(bibliotheque==null)
                {
                    response.status=404
                    render status: AUCUNE_RESSOURCE_TROUVEE
                }else{
                    boolean resultat = rechercheJsonEntete(request, CONTENT_APPLICATION)
                    //println("resultat " + resultat)
                    if(resultat)
                    {
                        response.status =200
                        response.setContentType(CONTENT_APPLICATION)
                        JsonBuilder builder = new JsonBuilder()
                        builder(
                                nom: bibliotheque.getName(),
                                id: bibliotheque.getId(),
                                address: bibliotheque.getAddress(),
                                yearCreated:bibliotheque.getYearCreated()
                        )
                        render builder, status:SUCCES_REQUETTE

                    }else {
                        response.status=406
                        render status: RESSOURCE_INDISPONIBLE_AU_FORMAT_ACCEPT
                    }
                }
                break

            case "DELETE":
                if(bibliotheque==null)
                {
                    response.status=404
                    render status: AUCUNE_RESSOURCE_TROUVEE, text:"L'entité à supprimer est introuvable"
                }else{
                    bibliotheque.delete()
                    response.status=200
                    render status: SUCCES_REQUETTE, text:"OK"
                }
                break
            case "PUT":
                if(bibliotheque==null)
                {

                    response.status=404
                    render status: AUCUNE_RESSOURCE_TROUVEE
                }else{
                    bibliotheque.setName(params.name)
                    bibliotheque.setAddress(params.address)
                    try{
                        bibliotheque.setYearCreated(Integer.parseInt(params.yearCreated))
                        if(!bibliotheque.save())
                        {
                            response.status=404
                            render status:  AUCUNE_RESSOURCE_TROUVEE
                        }else{
                            response.status=201
                            render status: RESSOURCE_CREE, text:"Success"
                        }

                    }catch(Exception ex)
                    {
                        response.status=404
                        render status:  AUCUNE_RESSOURCE_TROUVEE
                    }
                }
                break
            default:
                response.status=405
                render status: METHODE_NON_ACCEPTEE
        }
    }





    def boolean rechercheJsonEntete(def request, String recherche)
    {
        boolean trouve =false
        request.getHeaderNames().each {
            String paramAccept = String.valueOf(it)
            if(paramAccept.equals("accept"))
            {
                String valeur = String.valueOf(request.getHeader(it))
                //println(it+":"+request.getHeader(it)+ " value ")
                if(valeur.contains(recherche.toLowerCase()))
                {
                    trouve= true
                }

            }
        }

        return trouve
    }






    //
}
