package tprest

class UrlMappings {


    static mappings = {

        "/bibliotheque/$id"(controller: "Api", action: "bibliotheque")
        "/livre/$id"(controller: "Api", action: "livre")

        "/bibliotheques"(controller: "Api", action: "bibliotheques")
        "/livres"(controller: "Api", action: "livres")



        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
